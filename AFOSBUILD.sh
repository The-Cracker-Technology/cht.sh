curl https://cht.sh/:cht.sh > /opt/ANDRAX/bin/cht.sh

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Download cht... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
